from flask.ext.sqlalchemy import SQLAlchemy
from flask import Flask,current_app
from flask.ext.login import UserMixin
from sqlalchemy import Column, Integer, String
from werkzeug.security import generate_password_hash, check_password_hash
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
app = Flask(__name__)
db = SQLAlchemy(app)
db_name = "flask1"

app.config['SQLALCHEMY_DATABASE_URI'] ='mysql://root:srinath@localhost/'+db_name
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True
db = SQLAlchemy(app)


class Requesting(db.Model,UserMixin):
	__tablename__ = 'requests'
	id = db.Column(db.Integer, primary_key=True)
	email = db.Column(db.String(64))
	username = db.Column(db.String(64))
	landmark = db.Column(db.String(64))
	area = db.Column(db.String(64))
	contact = db.Column(db.String(64))
	item = db.Column(db.String(64))

	#bugs_id = db.relationship('Bugs', backref='bugs', lazy='dynamic')


	def generate_confirmation_token(self, expiration=3600):
		s = Serializer(current_app.config['SECRET_KEY'], expiration)
		return s.dumps({'confirm': self.id})	

	@property
	def password(self):
		raise AttributeError('password is not a readable attribute')

	@password.setter
	def password(self, password):
		self.password_hash = generate_password_hash(password)

	def confirm(self,token):
		s = Serializer(current_app.config['SECRET_KEY'])
		try:
			data = s.loads(token)
			print "printing data"
			print data
		except:
			return False
		if data['confirm'] != self.id:
			print "data is not equal"
			print self.id
			print data
			return False
		self.confirmed = True
		db.session.add(self)
		print "sesion is added"
		return True


	def verify_password(self, password):
		return check_password_hash(self.password_hash, password)

	def __repr__(self):
		return '<User %r>' % self.username


class User(UserMixin, db.Model):
	__tablename__ = 'users'
	id = db.Column(db.Integer, primary_key=True)
	email = db.Column(db.String(64))
	username = db.Column(db.String(64))
	#landmark = db.Column(db.String(64),  index=True)
	area = db.Column(db.String(64))
	contact = db.Column(db.String(64))
	item = db.Column(db.String(64))

	# contact = db.Column(db.String(64), unique=True, index=True)
	# role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))
	# password_hash = db.Column(db.String(128))
	# lined_in= db.Column(db.String(64))
	# bugs_solved = db.Column(db.Integer,default=0)
	# money_received = db.Column(db.Integer,default=0)
	# confirmed = db.Column(db.Boolean, default=False)
	#bugs = db.relationship('Bugs', backref='role', lazy='dynamic')


	def generate_confirmation_token(self, expiration=3600):
		s = Serializer(current_app.config['SECRET_KEY'], expiration)
		return s.dumps({'confirm': self.id})

	

	@property
	def password(self):
		raise AttributeError('password is not a readable attribute')

	@password.setter
	def password(self, password):
		self.password_hash = generate_password_hash(password)

	def confirm(self,token):
		s = Serializer(current_app.config['SECRET_KEY'])
		try:
			data = s.loads(token)
			print "printing data"
			print data
		except:
			return False
		if data['confirm'] != self.id:
			print "data is not equal"
			print self.id
			print data
			return False
		self.confirmed = True
		db.session.add(self)
		print "sesion is added"
		return True


	def verify_password(self, password):
		return check_password_hash(self.password_hash, password)

	def __repr__(self):
		return '<User %r>' % self.username



db.create_all()	